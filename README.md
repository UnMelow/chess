[![coverage report](https://gitlab.com/UnMelow/chess/badges/main/coverage.svg)](https://gitlab.com/UnMelow/chess/-/commits/main)

# Chess

Шахматы по сети.
Реализация через консоль, пользователь вводит команды и получает ответы.
Протокол TCP.
Изначально, нужно будет создать комнату или подключиться к существующей и начать игру.

## Команды

* Присоединение к комнате
* Получение информации об id пользователей и названии комнаты
* Начало игры
* Получение шахматной фигуры по положению на доске
* Движение шахматной фигуры
* Просмотр шахматной доски
* Выход из игры (сдаться)

#### Запрос: connect_to_room <room_name: str> \n

Ответ (одна из строк):

- new_room_created \n (если не найдена комната с переданным названием)
- successfully_connected_to_room \n
- room_filled_up \n
- can_not_connect_to_own_room \n

_Пример: connect_to_room some_amazing_name ---> new_room_created_

#### Запрос: info \n

Ответ (json): { user_id: <user_id: int>, opponent_id: <opponent_id: int | "null">, room_name: <room_name: str | "null">, user_to_make_move: <user_id: int | "null">, chess_colour: <colour: str | "null"> } \n

* user_id - id текущего пользователя
* opponent_id - id соперника ("null", если пользователь не находится в комнате)
* room_name - название комнаты ("null", если пользователь не находится в комнате)
* user_to_make_move - id пользователя, который сейчас должен сделать ход ("null", если нет игры)
* chess_colour - цвет фигур текущего пользователя

_Пример: info ---> {"user_id": 57813, "opponent_id": 57304, "room_name": "some_amazing_name", "user_to_make_move": 57813, "chess_colour": "white"}_

#### Запрос: start_game \n

Ответ (одна из строк):

- wait (соперник еще не отправил команду "start_game" и нужно его дождаться)
- success <chess_colour: "black" | "white"> \n (соперник тоже отправил команду "start_game", игра начинается, получен цвет фигур для текущего пользователя)
- no_opponent_found_in_the_room \n
- no_room_is_present \n

_Пример: start_game ---> success white_

#### Запрос: get <position: char, char> \n (получить шахматную фигуру по ее положению на доске)

`position - строка длины 2, содержит строго сначала одну букву (a, b, c, d, e, f, g или h),  затем одну цифру (1, 2, 3, 4, 5, 6, 7 или 8)`

Ответ (одна из строк):

- <piece: char> \n (символ фигуры)
- wrong_position \n

_Пример: get d1 ---> Q_

#### Запрос: move <start_position: char, char> <end_position: char, char> \n

`start_position, end_position - строки длины 2, содержат строго сначала одну букву (a, b, c, d, e, f, g или h),  затем одну цифру (1, 2, 3, 4, 5, 6, 7 или 8)`

Ответ (одна из строк):

- no_room_is_present \n
- game_did_not_start \n (если не получена команда start от всех игроков)
- no_opponent_found_in_the_room \n
- wait \n (пользователь уже сделал ход, нужно дождаться хода соперника)
- game_ended_you_won \n (пользователь уже победил)
- game_ended_you_lost \n (пользователь уже проиграл)
- illegal_move \n (невозможный ход)
- bad_command \n (ошибка в формате хода)
- success \n (после этого ответа пользователь переводится в режим ожидания, пока соперник не сделает свой ход)
- victory \n (ход пользователя поставил мат сопернику, пользователь выиграл, можно покидать игру)

_Пример: move b1 a3 ---> success_

#### Запрос: board \n (получить информацию о фигурах на игровой доске)

`Заглавные буквы обозначают белые фигуры, строчные - черные. `
`Фигуры: k, K - король; q, Q - ферзь; b, B - слон; n, N - конь; r, R - ладья; p, P - пешка; точка - отсутствие фигуры.`

Ответ (одна из строк):

- success <rnbqkbnr/pppppppp/......../......../......../......../PPPPPPPP/RNBQKBNR: str> \n
- no_game_is_present \n

_Пример: board ---> success rnbqkbnr/pppppppp/......../......../....P.../......../PPPP.PPP/RNBQKBNR_

#### Запрос: leave_room \n (сдаться)

Ответ (одна из строк):

- success \n
- no_room_is_present \n

_Пример: leave_room ---> success_
