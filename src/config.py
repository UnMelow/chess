from environs import Env

env = Env()
env.read_env()

HOST = env.str("HOST", "0.0.0.0")
PORT = env.int("PORT", 8080)
