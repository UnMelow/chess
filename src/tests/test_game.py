import pytest

from src.commands import start_game, get_board, make_move, get_piece_at_position, get_info


def test_start_game(create_room):
    create_room.connect_user(3)
    assert start_game(4) == 'no_room_is_present'
    assert start_game(1) == 'wait'
    assert start_game(3) == 'success black'


def test_get_board(start_game_conf):
    assert get_board(1) == 'success rnbqkbnr/pppppppp/......../......../......../......../PPPPPPPP/RNBQKBNR'
    assert get_board(3) == 'success rnbqkbnr/pppppppp/......../......../......../......../PPPPPPPP/RNBQKBNR'
    assert get_board(4) == 'no_game_is_present'


def test_make_move(start_game_conf):
    result = '{"user_id": 1, "opponent_id": 3, "room_name": "Room", "user_to_make_move": 3, "chess_colour": "white"}'
    assert make_move(1, 'a2', 'a3') == 'success'
    assert get_board(1) == 'success rnbqkbnr/pppppppp/......../......../......../P......./.PPPPPPP/RNBQKBNR'
    assert make_move(1, 'a3', 'a4') == 'wait'
    assert get_info(1) == result
    assert make_move(3, 'a7', 'a6') == 'success'
    assert get_board(3) == 'success rnbqkbnr/.ppppppp/p......./......../......../P......./.PPPPPPP/RNBQKBNR'
    assert make_move(1, 'a2', 'a3') == 'illegal_move'
    assert make_move(1, 'waf', 'qwe') == 'bad_command'


@pytest.mark.parametrize("position, piece", [
    ('a2', 'P'),
    ('d1', 'Q'),
    ('c4', '.'),
    ('b3', 'P'),
    ('a7', 'p')
]
                         )
def test_get_piece(start_game_conf, position, piece):
    make_move(1, 'b2', 'b3')
    assert get_piece_at_position(1, position) == piece


def test_checkmate(checkmate):
    assert make_move(3, 'd8', 'h4') == 'victory'
