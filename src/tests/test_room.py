from src.commands import get_info, connect_to_room
from src.loader import rooms_manager

from src.models.room import Room


def test_room(create_room):
    assert isinstance(create_room, Room)
    assert create_room.get_user_chess_colour(1) == 'white'
    assert connect_to_room(3, 'Room') == 'successfully_connected_to_room'
    assert not create_room.connect_user(2)
    assert connect_to_room(2, 'Room') == 'room_filled_up'
    assert connect_to_room(2, 'New_room') == 'new_room_created'
    assert rooms_manager.leave_room(2)
    assert create_room.get_user_chess_colour(3) == 'black'


def test_info_msg(create_room):
    info_1 = '{"user_id": 1, "opponent_id": null, "room_name": "Room", "user_to_make_move": 1, "chess_colour": "white"}'
    info_3 = '{"user_id": 3, "opponent_id": 1, "room_name": "Room", "user_to_make_move": 1, "chess_colour": "black"}'
    assert get_info(1) == info_1
    create_room.connect_user(3)
    assert get_info(3) == info_3


def test_utils(start_game_conf):
    assert (start_game_conf.get_opponent_id(1), start_game_conf.get_opponent_id(3)) == (3, 1)
    assert start_game_conf.show() == str(start_game_conf.game.board)
    assert start_game_conf == rooms_manager.get_room_by_name('Room')
    assert start_game_conf == rooms_manager.get_room_by_player_id(1)

