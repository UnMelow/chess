import pytest

from src.commands import start_game, make_move, connect_to_room
from src.loader import rooms_manager


@pytest.fixture()
def create_room(user1_id: int = 1, room_name: str = 'Room'):
    rooms_manager.create_new_room(room_name, user1_id)
    room = rooms_manager.rooms[0]

    yield room
    rooms_manager.leave_room(user1_id)


@pytest.fixture()
def start_game_conf(create_room, user1_id: int = 1, user2_id: int = 3):
    connect_to_room(user2_id, 'Room')
    start_game(user1_id)
    start_game(user2_id)

    yield create_room


@pytest.fixture()
def checkmate(start_game_conf):
    make_move(1, 'g2', 'g4')
    make_move(3, 'e7', 'e5')
    make_move(1, 'f2', 'f3')

    yield
