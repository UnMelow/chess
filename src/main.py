import os
import socket
import threading
from concurrent.futures import ThreadPoolExecutor

import config
from src import loader
# No inspection is added because these imports register command functions
# noinspection PyUnresolvedReferences
from src.commands import *


def handle_command(user_id: int, cmd: str, *args: str) -> str:
    try:
        command_func = loader.commands_dict.get(cmd)
        if command_func is None:
            raise ValueError('Unknown command')
        result = command_func(user_id, *args)
    except (TypeError, ValueError, AssertionError) as e:
        result = str(e)
    except StopIteration:
        result = ''
    return result


def handle_client(conn: socket.socket, addr: tuple):
    print(f'Using thread {threading.get_ident()} for client: {addr}')
    user_port = addr[1]
    read_file = conn.makefile(mode='r', encoding='utf-8')
    write_file = conn.makefile(mode='w', encoding='utf-8')
    write_file.write('Welcome to chess game!\n')
    write_file.flush()
    cmd = read_file.readline().strip()
    while cmd:
        result = handle_command(user_port, *cmd.split())
        if result == '':
            write_file.write('Goodbye!\n')
            write_file.flush()
            break
        write_file.write(result + '\n')
        write_file.flush()
        cmd = read_file.readline().strip()
    conn.close()


def main():
    print(f'Started process with PID={os.getpid()}')

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((config.HOST, config.PORT))
        s.listen(5)
        with ThreadPoolExecutor(max_workers=5) as executor:
            while True:
                conn, addr = s.accept()
                executor.submit(handle_client, conn, addr)


if __name__ == '__main__':
    main()
