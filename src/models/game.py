import chess

from src.models import GameMoveStatus


class Game:
    def __init__(self, user_id: int):
        """Игрок, который создал игру, ходит первый"""
        self.board = chess.Board()
        self.winner_id: int | None = None
        self.id_of_user_to_make_move = user_id

    def move(self, move: str, user_id: int, opponent_id: int) -> GameMoveStatus:
        """
        После мата игра не завершается и на любые ходы возвращается 'game_ended_you_won' или 'game_ended_you_lost'.
        Чтобы завершить игру, надо отправить команду leave_game, тогда она завершится у всех.
        """
        if self.board.is_checkmate():
            if user_id == self.winner_id:
                return GameMoveStatus.game_ended_you_won
            else:
                return GameMoveStatus.game_ended_you_lost

        try:
            self.board.push_uci(move)
        except Exception as e:
            e = str(e)
            if 'illegal' in e:
                return GameMoveStatus.illegal_move
            if 'expected uci string' in e:
                return GameMoveStatus.bad_command
            if 'invalid uci' in e:
                return GameMoveStatus.bad_command

        if self.board.is_checkmate():
            self.winner_id = user_id
            return GameMoveStatus.victory

        self.id_of_user_to_make_move = opponent_id
        return GameMoveStatus.success
