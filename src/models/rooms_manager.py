from src.models import ConnectToRoomStatus, RoomInfo, StartGameStatus, GameMoveStatus
from src.models.room import Room


class RoomsManager:
    def __init__(self):
        self.rooms = []

    def create_new_room(self, room_name: str, user_id: int):
        self.rooms.append(Room(name=room_name, user1_id=user_id))

    def get_room_by_name(self, room_name: str) -> Room | None:
        for room in self.rooms:
            if room.name == room_name:
                return room
        return None

    def get_room_by_player_id(self, user_id: int) -> Room | None:
        for room in self.rooms:
            if user_id in [room.user1_id, room.user2_id]:
                return room
        return None

    def connect_to_room(self, user_id: int, room_name: str) -> ConnectToRoomStatus:
        room = self.get_room_by_name(room_name)
        if room:
            if room.user1_id == user_id:
                return ConnectToRoomStatus.can_not_connect_to_own_room
            user_was_connected_to_room = room.connect_user(user_id)
            if user_was_connected_to_room:
                return ConnectToRoomStatus.success
            return ConnectToRoomStatus.room_filled_up

        room = self.get_room_by_player_id(user_id)
        if room:
            self.rooms.remove(room)

        self.create_new_room(room_name, user_id)
        return ConnectToRoomStatus.room_created

    def get_info(self, user_id: int) -> RoomInfo:
        room = self.get_room_by_player_id(user_id)
        if room:
            return RoomInfo(user_id=user_id, room_name=room.name,
                            opponent_id=room.get_opponent_id(user_id),
                            user_to_make_move=room.game.id_of_user_to_make_move,
                            chess_colour=room.get_user_chess_colour(user_id))
        return RoomInfo(user_id=user_id, room_name=None,
                        opponent_id=None, user_to_make_move=None,
                        chess_colour=None)

    def start_game(self, user_id: int) -> StartGameStatus:
        room = self.get_room_by_player_id(user_id)
        if room:
            return room.start_game(user_id)
        return StartGameStatus.no_room_is_present

    def get_piece_at_position(self, user_id: int, position: str) -> str | None:
        room = self.get_room_by_player_id(user_id)
        if room:
            return room.get_piece_at_position(position)

    def make_move(self, user_id: int, move: str) -> GameMoveStatus:
        room = self.get_room_by_player_id(user_id)
        if room:
            return room.make_move(user_id, move)
        return GameMoveStatus.no_room_is_present

    def get_board(self, user_id: int) -> str | None:
        room = self.get_room_by_player_id(user_id)
        if room:
            return room.show()

    def leave_room(self, user_id: int) -> bool:
        room = self.get_room_by_player_id(user_id)
        if room:
            self.rooms.remove(room)
        return room is not None
