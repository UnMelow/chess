import chess

from src.models import StartGameStatus, GameMoveStatus
from src.models.game import Game


class Room:
    def __init__(self, name: str, user1_id: int):
        """
        При создании комнаты сразу же создается игра.
        Игрок, создавший комнату, всегда играет за белых.
        Изначально второго игрока в комнате нет, поэтому у него id = None.
        """
        self.name = name
        self.user1_id = user1_id
        self.user2_id: int | None = None  # заглушка
        self.is_user1_ready = False
        self.is_user2_ready = False
        self.game = Game(user1_id)

    def show(self) -> str | None:
        if self.is_user1_ready and self.is_user2_ready:
            return str(self.game.board)

    def get_opponent_id(self, user_id: int) -> int | None:
        if self.user2_id == user_id:
            return self.user1_id
        if self.user1_id == user_id:
            return self.user2_id

    def start_game(self, user_id: int) -> StartGameStatus:
        if self.user2_id is None:
            return StartGameStatus.no_opponent_found_in_the_room
        if user_id == self.user1_id:
            self.is_user1_ready = True
            if self.is_user2_ready:
                return StartGameStatus.success_white
            return StartGameStatus.wait
        if user_id == self.user2_id:
            self.is_user2_ready = True
            if self.is_user1_ready:
                return StartGameStatus.success_black
            return StartGameStatus.wait

    def get_piece_at_position(self, position: str) -> str | None:
        if self.is_user1_ready and self.is_user2_ready:
            try:
                position_index = chess.parse_square(position)
            except ValueError:
                return None
            piece = self.game.board.piece_at(position_index)
            if piece:
                return piece.symbol()
            return '.'

    def get_user_chess_colour(self, user_id: int) -> str | None:
        if user_id == self.user1_id:
            return 'white'
        if user_id == self.user2_id:
            return 'black'

    def connect_user(self, user_id: int) -> bool:
        """Если комната существует, значит существует и игрок 1, поэтому проверяем только игрока 2"""
        if self.user2_id is not None:
            return False
        self.user2_id = user_id
        return True

    def make_move(self, user_id: int, move: str) -> GameMoveStatus:
        if not self.is_user1_ready or not self.is_user2_ready:
            return GameMoveStatus.game_did_not_start
        if self.user2_id is None:
            return GameMoveStatus.no_opponent_found_in_the_room
        if user_id != self.game.id_of_user_to_make_move:
            return GameMoveStatus.wait

        opponent_id = self.get_opponent_id(user_id)
        return self.game.move(move, user_id, opponent_id)
