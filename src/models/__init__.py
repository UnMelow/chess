import dataclasses
import enum


class GameMoveStatus(enum.Enum):
    game_ended_you_won = 0
    game_ended_you_lost = 1
    illegal_move = 2
    bad_command = 3
    victory = 4
    success = 5
    game_did_not_start = 6
    no_opponent_found_in_the_room = 7
    wait = 8
    no_room_is_present = 9


class StartGameStatus(enum.Enum):
    no_opponent_found_in_the_room = 0
    success_white = 1
    success_black = 2
    wait = 3
    no_room_is_present = 4


class ConnectToRoomStatus(enum.Enum):
    room_filled_up = 0
    room_created = 1
    success = 2
    can_not_connect_to_own_room = 3


@dataclasses.dataclass
class RoomInfo:
    __slots__ = ('user_id', 'opponent_id', 'room_name', 'user_to_make_move', 'chess_colour')
    user_id: int
    opponent_id: int | None
    room_name: str | None
    user_to_make_move: int | None
    chess_colour: str | None

    def to_dict(self) -> dict[str, int | str | None]:
        return dict(user_id=self.user_id,
                    opponent_id=self.opponent_id,
                    room_name=self.room_name,
                    user_to_make_move=self.user_to_make_move,
                    chess_colour=self.chess_colour)
