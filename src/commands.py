import functools
import json

from src import models
from .loader import commands_dict, rooms_manager


def register_command(name: str = None) -> callable:
    def inner(func: callable) -> callable:
        if name is None:
            commands_dict[func.__name__] = func
        else:
            commands_dict[name] = func
        return func

    return inner


def required_args(count: int) -> callable:
    def inner(func: callable) -> callable:
        @functools.wraps(func)
        def wrapped_func(*args, **kwargs):
            if len(args) + len(kwargs) == count:
                if all(map(lambda arg: arg is not None, [*args, *kwargs.values()])):
                    return func(*args, **kwargs)
            raise AssertionError(f'{func.__name__} command takes exactly {count - 1} argument(s)')

        return wrapped_func

    return inner


@register_command()
@required_args(count=2)
def connect_to_room(user_id: int, room_name: str) -> str:
    status = rooms_manager.connect_to_room(user_id, room_name)
    match status:
        case models.ConnectToRoomStatus.room_created:
            return 'new_room_created'
        case models.ConnectToRoomStatus.room_filled_up:
            return 'room_filled_up'
        case models.ConnectToRoomStatus.success:
            return 'successfully_connected_to_room'
        case models.ConnectToRoomStatus.can_not_connect_to_own_room:
            return 'can_not_connect_to_own_room'


@register_command(name='info')
@required_args(count=1)
def get_info(user_id: int) -> str:
    room_info = rooms_manager.get_info(user_id)
    return json.dumps(room_info.to_dict())


@register_command()
@required_args(count=1)
def start_game(user_id: int) -> str:
    status = rooms_manager.start_game(user_id)
    match status:
        case models.StartGameStatus.success_white:
            return 'success white'
        case models.StartGameStatus.success_black:
            return 'success black'
        case models.StartGameStatus.wait:
            return 'wait'
        case models.StartGameStatus.no_opponent_found_in_the_room:
            return 'no_opponent_found_in_the_room'
        case models.StartGameStatus.no_room_is_present:
            return 'no_room_is_present'


@register_command(name='get')
@required_args(count=2)
def get_piece_at_position(user_id: int, position: str) -> str:
    piece_char = rooms_manager.get_piece_at_position(user_id, position)
    if piece_char:
        return piece_char
    return 'wrong_position'


@register_command(name='move')
@required_args(count=3)
def make_move(user_id: int, start_position: str, end_position: str) -> str:
    status = rooms_manager.make_move(user_id, start_position + end_position)
    return status.name


@register_command(name='board')
@required_args(count=1)
def get_board(user_id: int) -> str:
    board = rooms_manager.get_board(user_id)
    if board:
        return 'success ' + board.replace('\n', '/').replace(' ', '')
    return 'no_game_is_present'


@register_command()
@required_args(count=1)
def leave_room(user_id: int) -> str | None:
    room_was_deleted = rooms_manager.leave_room(user_id)
    if room_was_deleted:
        raise StopIteration
    return 'no_room_is_present'
